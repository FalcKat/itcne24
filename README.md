# ITCNE24 :tulip:

##### Dies ist das E-Portfolio zum HF Studienlehrgang Informatikerin Cloud-Native Engineering. 

$`\textcolor{purple}{\text{Dauer}}`$ 

Von Februar 2024
Bis Februar 2027

$`\textcolor{purple}{\text{Klasse}}`$ 

ITCNE24

$`\textcolor{purple}{\text{Ort}}`$

Technische Berufschule Zürich 

## $`\textcolor{Magenta}{\text{Inhaltsverzeichniss}}`$


- #### [Module](Inhalt/Module/README.md)
    - [MAAS](Inhalt/Module/MAAS/README.md)
    - [IAC](Inhalt/Module/IAC/README.md)
    - [AWS](Inhalt/Module/AWS/README.md)	
    - [PRJ](Inhalt/Module/PRJ/README.md)
    - [NWA](Inhalt/Module/NWA/README.md)
    - [Azure](Inhalt/Module/Azure/README.md)
    - [PE](Inhalt/Module/PE/README.md)
  
- #### [Code](Code/Skripting/README.md)
	- [Bash](Bash)
	- [Python](Python)

- #### [How to / Knowledge](Inhalt/Knowledge/README.md)
- #### [Links](Inhalt/Links/README.md)


# :cat: 



- - -
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>

- - -
